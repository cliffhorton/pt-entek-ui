#High-Level UI Testplan
-----------------------
The NetSense(NS) User-Interface(UI) testing of the 'Pass-Through Load Control Service(LCS)' feature covers the following requirements:
  * Written in Python (may include powershell 5.0+ helper scripts)
  * Executes from an end-user point of view 
  * Utilizes Splinter v0.10.0(a Python3.5+ Selenium based automation tool) 
  * Python 3.5+, Chrome <74.0+ matched to ChromeDriver 74.0+>
  * Executes on Demandsites(DS), an openADR standard for Demand Response Systems
  * Validation of field length - common to DS & NS (specs?) i.e length/types must match...
  * End-to-End Testing, round-trip DS->NS->DS testing


##Software Versioning:
----------------------
* NetSense 11.04
* NMS ---
* NOC ---
* Entek LCS 4.10+
* mNIC ---
* DemandSites ---
...
... (add as needed)

##Specifications:
See pt-entek-lcs -> HLTP.md
Find Validation references...

##Test Scenarios:
Name          	Description
----            -----------
*DemandSites>*
CreateEvt	    Create via DemandSites an event to trigger a *sendToDevice* event to a NetSense AMI Server.
LogEvtCreated	Verify via events, logs, or inspection a CreateEvt message (*sendToDevice*) has been requested.
		        Verify via inspection, the *sendToDevice* XML(v??) call is formed as specifications state.
                Note: Testing may be repeated if specifications call for multiple possibilities.

*NetSense>*
AcceptMessage	Accept of *sendToDevice* pass-through payload is processed as successful (verify via Events, Logs or inspection)
TestMessage	    Verify Netsense responds appropriately to bounds testing
TimeofMessage	Verify the XML payload response is not malformed and the XML version is (assumed same as request)
TestPayload	    Verify the *sendToDevice* payload is delivered and relay states are triggered as expected
ViewJobMonitor	Via Job Monitor, Logs or Events verify the return XML call back message is traceable
SendCallback	Verify the callback and XML version ??login back in or remain logged in??

*DemandSites>*	
AcceptCallBack	DemandSites accepts and logs the Soap XML message
MessageSaved	The message is stored/available as specified! CorrelationId matches.
